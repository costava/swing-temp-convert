// BSD Zero Clause License
//
// Copyright (C) 2024 by costava
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
// REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
// INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
// LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
// OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
// PERFORMANCE OF THIS SOFTWARE.

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.*;

enum InputType {
	FAHRENHEIT, CELSIUS
}

class FieldKeyListener implements KeyListener {
	private InputType inputType;
	private JTextField input;
	private JTextField responseInput;
	private JLabel label;

	public FieldKeyListener(
		InputType inType,
		JTextField inInput,
		JTextField inResp,
		JLabel inL)
	{
		inputType = inType;
		input = inInput;
		responseInput = inResp;
		label = inL;
	}

	void UpdateAll() {
		try {
			final double inputTemp = Double.parseDouble(input.getText());

			if (inputType == InputType.FAHRENHEIT) {
				final double responseTemp = (inputTemp - 32.0) * 5.0 / 9.0;
				responseInput.setText(Double.toString(responseTemp));

				label.setText(
				Double.toString(inputTemp) +
				"° Fahrenheit is " +
				Double.toString(responseTemp) +
				"° Celsius.");
			}
			else if (inputType == InputType.CELSIUS) {
				final double responseTemp = inputTemp * 9.0 / 5.0 + 32.0;
				responseInput.setText(Double.toString(responseTemp));

				label.setText(
				Double.toString(responseTemp) +
				"° Fahrenheit is " +
				Double.toString(inputTemp) +
				"° Celsius.");
			}
			else {
				assert false;
			}
		}
		// If the parsing fails:
		catch(NumberFormatException nfe) {
			if (inputType == InputType.FAHRENHEIT) {
				label.setText("INVALID FAHRENHEIT INPUT.");
			}
			else if (inputType == InputType.CELSIUS) {
				label.setText("INVALID CELSIUS INPUT.");
			}
			else {
				assert false;
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// Nothing.
	}

	@Override
	public void keyReleased(KeyEvent e) {
		UpdateAll();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// Nothing.
	}
}

public class TempConvert {

	public static void main(String[] args) {
		JFrame f = new JFrame("Temperature Conversion");

		JPanel colPanel = new JPanel();
		f.add(colPanel);
		GridLayout colPanelLayout = new GridLayout(2, 1);
		colPanel.setLayout(colPanelLayout);

		JPanel rowPanel = new JPanel();
		colPanel.add(rowPanel);

		JLabel summaryText = new JLabel("Summary", JLabel.CENTER);
		colPanel.add(summaryText);

		JLabel fahr = new JLabel("Fahrenheit", JLabel.CENTER);
		rowPanel.add(fahr);

		final int inputWidthColumns = 8;

		JTextField fahrInput = new JTextField("-40", inputWidthColumns);
		fahrInput.setEditable(true);
		rowPanel.add(fahrInput);

		JLabel equalsText = new JLabel("=", JLabel.CENTER);
		rowPanel.add(equalsText);

		JTextField celsInput = new JTextField("-40", inputWidthColumns);
		celsInput.setEditable(true);
		rowPanel.add(celsInput);

		JLabel cels = new JLabel("Celsius", JLabel.CENTER);
		rowPanel.add(cels);

		FieldKeyListener fahrKeyListener = new FieldKeyListener(
			InputType.FAHRENHEIT, fahrInput, celsInput, summaryText);
		fahrInput.addKeyListener(fahrKeyListener);

		FieldKeyListener celsKeyListener = new FieldKeyListener(
			InputType.CELSIUS, celsInput, fahrInput, summaryText);
		celsInput.addKeyListener(celsKeyListener);

		// Could pick either fahr or cels KeyListener,
		// but only need to call for one of them.
		fahrKeyListener.UpdateAll();

		{
			// Centers the top left corner of the window.
			// f.setLocationRelativeTo(null);

			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

			final int initialWidth = 500;
			final int initialHeight = 150;

			f.setSize(initialWidth, initialHeight);
			f.setLocation(
				((int)screenSize.getWidth() - initialWidth) / 2,
				((int)screenSize.getHeight() - initialHeight) / 2
			);
		}

		f.setVisible(true);
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent event) {
				System.exit(0);
			}
		});
	}
}
