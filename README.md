# Temperature Conversion GUI

A GUI program written in Java for converting temperature between Fahrenheit and Celsius.

![Screenshot of the program running on Linux including the window title bar. There is a labeled input field for each of Fahrenheit and Celsius.](https://files.catbox.moe/jzr1fq.png)

Tested with javac 21.

## License

BSD Zero Clause License. See file `LICENSE.txt`.
