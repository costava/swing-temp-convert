.PHONY: all
all: bin/TempConvert.class

.PHONY: run
run: bin/TempConvert.class
	java -cp bin TempConvert

bin:
	mkdir bin

bin/TempConvert.class: TempConvert.java | bin
	javac -encoding UTF-8 -d bin TempConvert.java

.PHONY: clean
clean:
	rm -f bin/*.class
